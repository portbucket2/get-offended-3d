﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNPC : MonoBehaviour
{
    [SerializeField] List<GameObject> heads;
    [SerializeField] List<GameObject> bodies;

    void Start()
    {
        int max = heads.Count;
        for (int i = 0; i < max; i++)
        {
            heads[i].SetActive(false);
        }
        int max2 = bodies.Count;
        for (int i = 0; i < max2; i++)
        {
            bodies[i].SetActive(false);
        }
        int rand = Random.Range(0, max);
        heads[rand].SetActive(true);
        bodies[rand].SetActive(true);
    }

    
}
