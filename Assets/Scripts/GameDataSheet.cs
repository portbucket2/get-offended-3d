﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameDataSheet", order = 1)]
public class GameDataSheet : ScriptableObject
{
    public string detail;

    public List<int> numberOfTries;
    public List<SegmentQuestions> segmentQuestions;
    public List<LevelEndResponse> levelEndResponse;

    [Header("Debug Auto fill")]
    private List<LevelQuestions> levelQuestions;

    private void Awake()
    {
               
    }

    
    public SegmentQuestions GetRandomSegment()
    {
        int max = segmentQuestions.Count;
        int rand = Random.Range(0, max);

        return segmentQuestions[rand];
    }
}


[System.Serializable]
public struct SegmentQuestions
{
    public string convoCounter;
    public string convoStart;
    public string convoStartWhy;
    public string convoEndRight;
    public string ConvoEndLeft;
    public string rightResponse;
    public string leftResponse;
    public Sprite rightSprite;
    public Sprite leftSprite;
    public Sprite leftSpriteExtra;
    public string rightSpriteText;
    public string leftSpriteText;
    public string leftSpriteExtraText;
}
[System.Serializable]
public struct LevelEndResponse
{
    public string levelEndRight;
    public string levelEndLeft;
}

    [System.Serializable]
public struct LevelQuestions
{
    public string level;
    public string levelTopic;
    public int numberOfTries;
    public int strikesToWin;
    public List<SegmentQuestions> questions;
    public string levelEndRight;
    public string levelEndLeft;
}