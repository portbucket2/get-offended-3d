﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DitzelGames.FastIK;

public class CharacterController : MonoBehaviour
{
    [SerializeField] Animator playerAnimator;
    [SerializeField] Animator playerFaceAnimator;
    [SerializeField] Animator npcAnimator;
    [SerializeField] Animator npcFaceAnimator;

    [SerializeField] ParticleSystem dustParticle;
    [SerializeField] ParticleSystem hitParticle;
    [Header("IK part")]
    [SerializeField] FastIKFabric armIK;
    [SerializeField] FastIKLook headIK;
    [SerializeField] Transform ikTarget;
    [SerializeField] Transform selfieCamera;


    readonly string IDLE = "idle";
    readonly string ENTRY = "entry";
    readonly string TALK = "talk";
    readonly string HAPPY = "happy";
    readonly string WRONG = "wrong";
    readonly string VICTORY = "victory";
    readonly string DEFEAT = "defeat";
    readonly string REACT = "react";
    readonly string SELFIE = "selfie";

    WaitForSeconds WAIT = new WaitForSeconds(2f);

    private bool followCam = false;
    private float isFast = 0f;

    private void Awake()
    {
        armIK.enabled = false;
        headIK.enabled = false;
        armIK.Target = ikTarget;
        headIK.Target = ikTarget;
    }
    void Start()
    {
        

    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Time.timeScale = 1f;
            isFast = 0f;
        }
        if (Input.GetMouseButton(0))
        {
            
            if (isFast > 0.5f)
            {
                Time.timeScale = 2f;
            }
            else
            {
                isFast += Time.deltaTime;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            Time.timeScale = 1f;
            isFast = 0f;
        }
    }
    private void LateUpdate()
    {
        if (followCam)
        {
            headIK.transform.LookAt(selfieCamera.position);
        }
    }


    public void PlayerIdle() { playerAnimator.SetTrigger(IDLE); playerFaceAnimator.SetTrigger(IDLE); }
    public void NPCIdle() { npcAnimator.SetTrigger(IDLE); npcFaceAnimator.SetTrigger(IDLE); }

    public void PlayerEntry() { playerAnimator.SetTrigger(ENTRY); playerFaceAnimator.SetTrigger(ENTRY); dustParticle.Play(); NPCReact(); }

    public void NPCReact() { npcAnimator.SetTrigger(REACT); npcFaceAnimator.SetTrigger(REACT); }

    public void PlayerTalk() { playerAnimator.SetTrigger(TALK); playerFaceAnimator.SetTrigger(TALK); }
    public void NPCTalk() { npcAnimator.SetTrigger(TALK); npcFaceAnimator.SetTrigger(TALK); }

    public void PlayerHappy() { playerAnimator.SetTrigger(HAPPY); playerFaceAnimator.SetTrigger(HAPPY); }
    public void NPCHappy() { npcAnimator.SetTrigger(HAPPY); npcFaceAnimator.SetTrigger(TALK); }

    public void PlayerWrong() { playerAnimator.SetTrigger(WRONG); playerFaceAnimator.SetTrigger(WRONG); }
    public void NPCWrong() { npcAnimator.SetTrigger(WRONG); npcFaceAnimator.SetTrigger(WRONG); }

    public void PlayerVictory() { playerAnimator.SetTrigger(VICTORY); playerFaceAnimator.SetTrigger(VICTORY); }
    public void NPCVictory() { npcAnimator.SetTrigger(VICTORY); npcFaceAnimator.SetTrigger(VICTORY); }

    public void PlayerDefeat() { playerAnimator.SetTrigger(DEFEAT); playerFaceAnimator.SetTrigger(DEFEAT); hitParticle.Play(); }
    public void NPCDefeat() { npcAnimator.SetTrigger(DEFEAT); npcFaceAnimator.SetTrigger(DEFEAT); NPCWAlkAway(); }

    public void PlayerHappyFace() { playerFaceAnimator.SetTrigger(SELFIE); }

    public void SelfieTime()
    {
        armIK.enabled = true;
        headIK.enabled = false;
        PlayerHappyFace();
        headIK.transform.DOLookAt(selfieCamera.position, 0.5f).OnComplete( ()=> followCam = true);
    }
    
    void NPCWAlkAway()
    {
        StartCoroutine(WalkAwayRoutine());
    }
    IEnumerator WalkAwayRoutine()
    {
        Transform npc = npcAnimator.transform;
        yield return WAIT;
        npc.DOMove( npc.position + Vector3.right * 10f, 1f);
    }
}
