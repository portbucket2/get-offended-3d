﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.faithstudio.SDK;
//using LionStudios;

public class AnalyticsController : MonoBehaviour
{
    public static AnalyticsController analyticsController;

    int levelcount = 1;

    GameManager gameManager;
    private void Awake()
    {
        levelcount = 1;
        analyticsController = this;
        gameManager = transform.GetComponent<GameManager>();
    }
    private void Start()
    {
        levelcount = gameManager.GetDataManager().GetGamePlayer.levelsCompleted;
    }

    public static AnalyticsController GetController()
    {
        return analyticsController;
    }

    public void LevelStarted()
    {
        //LionStudios.Analytics.Events.LevelStarted(levelcount);
        //AdjustSDKManager.Instance.InvokePreConfigEvent(AdjustEventTrack.test_event_delete);
        FacebookAnalyticsManager.Instance.FBALevelStart(levelcount);
    }
    public void LevelEnded()
    {
        //LionStudios.Analytics.Events.LevelComplete(levelcount);
        FacebookAnalyticsManager.Instance.FBALevelComplete(levelcount);
        levelcount++;
        SaveGame();
    }
    public void LevelFailed()
    {
        //LionStudios.Analytics.Events.LevelFailed(levelcount);
        FacebookAnalyticsManager.Instance.FBALevelFailed(levelcount);
    }
    public void SaveGame()
    {
        GamePlayer gp = new GamePlayer();
        gp.name = "";
        gp.id = 1;
        gp.levelsCompleted = levelcount;
        gp.totalCoins = 0;// GameController.GetController().GetTotalCoins();
        gp.lastPlayedLevel = gameManager.GetlevelCount();
        gp.handTutorialShown = true;

        gameManager.GetDataManager().SetGameplayerData(gp);
    }

}
