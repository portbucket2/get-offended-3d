﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    [SerializeField] bool tutorialShown = false;

    [Header("For Debugging")]
    public int levelcount = 0;
    public int totalScenes = 0;
    public int levelProgressionCount = 0;
    [SerializeField] DataManager dataManager;
    [SerializeField] GamePlayer gamePlayer;
    static bool titleSeen = false;


    [SerializeField] GameDataSheet data;
    [Header("Debug Auto fill")]
    [SerializeField] private List<LevelQuestions> levelQuestions;


    private void Awake()
    {
        gameManager = this;
        DontDestroyOnLoad(this.gameObject);
        dataManager = GetComponent<DataManager>();
        gamePlayer = new GamePlayer();


        SetData();
    }
    void Start()
    {
        AskPermissions();

        totalScenes = SceneManager.sceneCountInBuildSettings;
        
        gamePlayer = dataManager.GetGamePlayer;
        if (gamePlayer.handTutorialShown)
        {
            TutorialSeen();
        }
        //Load last played level
        levelcount = gamePlayer.lastPlayedLevel;
        //levelcount = 0;
        SceneManager.LoadScene(1);
    }

    public static GameManager GetManager()
    {
        return gameManager;
    }
    public void GotoNextStage()
    {
        if (levelcount < GetLevelCount() - 1)
            levelcount++;
        else
        {
            levelcount = 0;
            SetData();
        }

        SceneManager.LoadScene(1);

        levelProgressionCount++;



    }
    public int GetLevelProgressionCount()
    {
        return levelProgressionCount;
    }
    public void ResetStage()
    {
        SceneManager.LoadScene(1);
    }
    public int GetlevelCount()
    {
        return levelcount;
    }
    public bool TutorialAlreadySeen()
    {
        return tutorialShown;
    }
    public void TutorialSeen()
    {
        tutorialShown = true;
    }
    public DataManager GetDataManager()
    {
        return dataManager;
    }
    public bool IsTitleSeen()
    {
        return titleSeen;
    }
    public void TitleSeen()
    {
        titleSeen = true;
    }

    public LevelQuestions GetLevelQuestions(int level) { return levelQuestions[level]; }

    public int GetLevelCount() { return levelQuestions.Count; }


    private void AskPermissions()
    {
#if UNITY_ANDROID

        //Requesting WRITE_EXTERNAL_STORAGE and CAMERA permissions simultaneously
        AndroidRuntimePermissions.Permission[] result = AndroidRuntimePermissions.RequestPermissions("android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE");
        if (result[0] == AndroidRuntimePermissions.Permission.Granted && result[1] == AndroidRuntimePermissions.Permission.Granted)
            Debug.Log("We have all the permissions!");
        else
            Debug.Log("Some permission(s) are not granted...");

#endif
    }

    void SetData()
    {
        List<LevelQuestions> temp = new List<LevelQuestions>();
        temp = levelQuestions;
        levelQuestions = new List<LevelQuestions>();
        int max = data.numberOfTries.Count;
        Debug.Log("number of tries: " + max);
        for (int i = 0; i < max; i++)
        {
            LevelQuestions tempSmall = new LevelQuestions();
            tempSmall.level = "Level: " + i;
            tempSmall.levelTopic = "flirt";
            tempSmall.numberOfTries = data.numberOfTries[i];
            tempSmall.strikesToWin = data.numberOfTries[i];

            tempSmall.levelEndRight = data.levelEndResponse[i].levelEndRight;
            tempSmall.levelEndLeft = data.levelEndResponse[i].levelEndLeft;

            int mm = tempSmall.numberOfTries;
            List<SegmentQuestions> tempQues = new List<SegmentQuestions>();
            for (int j = 0; j < mm; j++)
            {
                tempQues.Add(data.GetRandomSegment());
            }

            tempSmall.questions = tempQues;


            levelQuestions.Add(tempSmall);
        }
    }
}
