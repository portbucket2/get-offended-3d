﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CancelMeter : MonoBehaviour
{
    [SerializeField] Image meterValueImage;
    [SerializeField] Transform strikePanel;
    [SerializeField] GameObject dotImagePrefab;

    [SerializeField] Sprite yellowDot;
    [SerializeField] Sprite greenDot;
    [SerializeField] Sprite redDot;

    [Header("Debug")]
    [SerializeField] List<Image> strikeImages;
    [SerializeField] int[] strikeImagesColor;
    [SerializeField] HorizontalLayoutGroup layoutGroup;
    // yellow = -1; red = 0; green = 1

    
    [SerializeField] int totalSegments = 0;
    [SerializeField] int currentSegment = 0;

    private void Awake()
    {
        layoutGroup = strikePanel.GetComponent<HorizontalLayoutGroup>();
    }

    void Start()
    {
        
    }
    public void SetCancelMeter(int _totalSegments)
    {
        if (_totalSegments > 2)
        {
            layoutGroup.padding.left = 10;
            layoutGroup.padding.right = 10;
        }
        else
        {
            layoutGroup.padding.left = 50;
            layoutGroup.padding.right = 50;
        }
        strikeImages = new List<Image>();

        int max = _totalSegments;
        for (int i = 0; i < max; i++)
        {
            GameObject gg = Instantiate(dotImagePrefab, strikePanel);
            strikeImages.Add(gg.GetComponent<Image>());
        }
        for (int i = 0; i < max; i++)
        {
            strikeImages[i].sprite = yellowDot;
        }


        totalSegments = _totalSegments;
        strikeImagesColor = new int[totalSegments];
        for (int i = 0; i < totalSegments; i++)
        {
            strikeImagesColor[i] = -1;
        }
        
    }
    public void CompleteBar()
    {
        meterValueImage.DOFillAmount(1f, 0.5f).SetEase(Ease.OutSine);
    }

    public void UpdateMeterValue(int _point,int _segmentCount, bool isRight)
    {
        float temp =(float) _point / totalSegments;

        meterValueImage.DOFillAmount(temp, 0.5f).SetEase(Ease.OutSine);

        if (isRight)
        {
            strikeImagesColor[_segmentCount] = 1;
        }
        else
        {
            strikeImagesColor[_segmentCount] = 0;
        }
        

        int max = strikeImagesColor.Length;
        
        for (int i = 0; i < max; i++)
        {
            if (strikeImagesColor[i] == 0)
            {
                strikeImages[i].sprite = redDot;
            }
            else if (strikeImagesColor[i] == 1)
            {
                strikeImages[i].sprite = greenDot;
            }
            else if (strikeImagesColor[i] == -1)
            {
                strikeImages[i].sprite = yellowDot;
            }
        }
    }
}
