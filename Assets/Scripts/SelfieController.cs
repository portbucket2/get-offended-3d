﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Cinemachine;

public class SelfieController : MonoBehaviour
{
    [SerializeField] GameObject selfieUI;
    [SerializeField] Button selfieButton;
    [SerializeField] CinemachineVirtualCamera mainCam;
    [SerializeField] CinemachineVirtualCamera ssCam;
    [SerializeField] Transform selfieStick;
    [SerializeField] CaptureScreen captureScreen;
    [SerializeField] CharacterController characterController;
    [SerializeField] Transform lookAtTarget;
    [SerializeField] VariableJoystick joystick;
    [SerializeField] float camSpeed = 0.3f;
    [SerializeField] float clampDistance = 30f;


    Vector3 startPosition;
    float horizontal;
    float vertical;
    bool isMainCam = true;
    bool isSelfieTime = false;

    void Start()
    {
        selfieButton.onClick.AddListener(delegate
        {
            Debug.Log("selfie button");
            selfieUI.SetActive(false);
            captureScreen.TakeSelfie();
            selfieButton.interactable = false;
        });
        startPosition = transform.position;
        //Debug.Log("angle " + selfieStick.localEulerAngles);
        //Invoke("SelfieTime", 1f);
    }

    public void SelfieTime()
    {
        isSelfieTime = true;
        selfieButton.interactable = true;
        joystick.gameObject.SetActive(true);
        selfieUI.SetActive(true);
        CameraChange();
        characterController.SelfieTime();

        //characterController.PlayerVictory();
        //characterController.NPCVictory();
    }
    public void SelfieComplete()
    {
        isSelfieTime = false;
        selfieUI.SetActive(false);
        CameraChange();
        joystick.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isSelfieTime)
        {

        }
        if (Input.GetMouseButton(0) && isSelfieTime)
        {
            horizontal = joystick.Horizontal * camSpeed;
            vertical = joystick.Vertical * camSpeed;
            // 6 to -15 y axis
            // -15 to 20 on x axis
            Vector3 rotation;
            rotation = new Vector3(Mathf.Clamp(selfieStick.localEulerAngles.x + vertical, 0f, 25f),
                Mathf.Clamp(selfieStick.localEulerAngles.y - horizontal, 0f, 20f), 0f);

            selfieStick.localEulerAngles = rotation;
            //Debug.Log("angle :" + selfieStick.localEulerAngles);
            transform.LookAt(lookAtTarget);
        }
    }
    void CameraChange()
    {
        if (isMainCam)
        {
            isMainCam = !isMainCam;
            mainCam.Priority = 0;
        }
        else
        {
            isMainCam = !isMainCam;
            mainCam.Priority = 2;
        }
    }
}
