﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TypeWriterEffect : MonoBehaviour
{
    [Range(0f, 0.15f)]
    [SerializeField] float gap = 0.05f;
    [Header("Debug: ")]
    [SerializeField] TextMeshProUGUI text;
    [SerializeField] string narrative;
    WaitForSeconds WAIT;
    private void Awake()
    {
        WAIT = new WaitForSeconds(gap);
        text = GetComponent<TextMeshProUGUI>();
    }
    
    public void SetColor(Color _color)
    {
        text.color = _color;
    }
    public void ShowMessage(string _narrative)
    {
        narrative = _narrative;
        text.text = "";
        StartCoroutine(PlayText());
    }

    IEnumerator PlayText()
    {
        foreach (char c in narrative)
        {
            text.text += c;
            yield return WAIT;
        }
    }
}
