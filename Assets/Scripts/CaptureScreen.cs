﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CaptureScreen : MonoBehaviour
{
    [SerializeField] Animator screenFlash;    

    [Header("Debug")]
    [SerializeField] SelfieController selfieController;
    [SerializeField] CanvasGroup canvasQA;

    Texture2D capturedTexture;

    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAIT = new WaitForSeconds(1f);
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    GameController gameController;
    private void Awake()
    {
        selfieController = GetComponent<SelfieController>();
    }
    void Start()
    {
        gameController = GameController.GetController();
        canvasQA = gameController.GetUI().GetComponent<CanvasGroup>();
        
    }

    
    public void SelfieMode()
    {
        selfieController.SelfieTime();

        canvasQA.alpha = 0;
    }
    public void TakeSelfie()
    {
        StartCoroutine(ScreenShotRoutine());
    }
    IEnumerator ScreenShotRoutine()
    {
        gameController.GetCharacterController().PlayerHappyFace();
        yield return WAIT;
        TakeScreenShot();
        yield return WAIT;
        selfieController.SelfieComplete();
        canvasQA.alpha = 1;
        gameController.GetQuestionsController().SelfieComplete();
    }
    void TakeScreenShot()
    {
        //Debug.Log("screen shot taken");
        if (Application.isEditor)
        {
            ScreenCapture.CaptureScreenshot(Application.persistentDataPath + "/sample.png");
        }
        else
        {
            ScreenCapture.CaptureScreenshot("/sample.png");
        }
        //ScreenCapture.CaptureScreenshot("/sample.png");
        screenFlash.SetTrigger("flash");
    }
    public Sprite GetScreenShot()
    {
        //image.gameObject.SetActive(true);
        capturedTexture = LoadPNG(Application.persistentDataPath + "/sample.png");
        Sprite sp = null;
        //#if UNITY_EDITOR
        
        //#endif
        if (capturedTexture)
            sp = Sprite.Create(capturedTexture, new Rect(0.0f, 0.0f, capturedTexture.width, capturedTexture.height), new Vector2(0.5f, 0.5f), 10.0f);

        return sp;
    }

    public Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            //Debug.Log("File exists ");
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }
    private void ShowImage()
    {
        
    }
}
