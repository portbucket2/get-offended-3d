﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;
    [Header("Elements")]
    [SerializeField] UIController UIController;
    [SerializeField] CharacterController characterController;
    [SerializeField] CancelMeter cancelMeter;
    [SerializeField] CaptureScreen captureScreen;
    [SerializeField] QuestionsController questionsController;

    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }

    void Start()
    {
        
    }

    public UIController GetUI() { return UIController; }
    public CharacterController GetCharacterController() { return characterController; }
    public CancelMeter GetCancelMeter() { return cancelMeter; }
    public CaptureScreen GetCaptureScreen() { return captureScreen; }
    public QuestionsController GetQuestionsController() { return questionsController; }

}
