﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;

public class QuestionsController : MonoBehaviour
{
    
    [SerializeField] int segmentCount = 0;
    [SerializeField] bool isAnswerRight = false;

    [SerializeField] TypeWriterEffect playerLinesText;
    [SerializeField] TypeWriterEffect npcResponceText;
    [SerializeField] TypeWriterEffect whyText;
    [SerializeField] TypeWriterEffect answerText;

    [SerializeField] GameObject pickUpLineStartPanel;
    [SerializeField] GameObject npcResponsePanel;
    [SerializeField] GameObject whyPanel;

    [SerializeField] GameObject levelCompletePanel;
    [SerializeField] Image screenShotImage;
    [SerializeField] GameObject levelFailedPanel;

    
    [SerializeField] TextMeshProUGUI dayCount;
    [SerializeField] TextMeshProUGUI levelCompleteText;
    [SerializeField] TextMeshProUGUI likes;
    [SerializeField] TextMeshProUGUI comments;
    [SerializeField] TextMeshProUGUI shares;

    [SerializeField] Image emojiImage1;
    [SerializeField] Image emojiImage2;
    [SerializeField] Image emojiImage3;

    [SerializeField] TextMeshProUGUI emojiText1;
    [SerializeField] TextMeshProUGUI emojiText2;
    [SerializeField] TextMeshProUGUI emojiText3;

    [SerializeField] Button emojiButton1;
    [SerializeField] Button emojiButton2;
    [SerializeField] Button emojiButton3;

    [SerializeField] GameObject emojiBGtray;


    [SerializeField] Button nextLeveButton;
    [SerializeField] Button resetLevelButton;

    
    [SerializeField] GameObject titlePanel;
    [SerializeField] Button startButton;
    [SerializeField] GameObject tutorialPanel;
    [SerializeField] Transform playerHead;
    [SerializeField] Transform npcHead;

    [SerializeField] List<ParticleSystem> celebrateParticles;

    [Header("Debug: ")]
    [SerializeField] bool isUpperRightAnswer = true;
    [SerializeField] bool isPreviousRight = true;
    [SerializeField] int totalSegments = 0;
    [SerializeField] int currentLevel = 0;
    [SerializeField] int point = 0;
    [SerializeField] LevelQuestions levelQuestions;

    bool titleSeen = false;

    GameController gameController;
    CharacterController characterController;

    
    WaitForSeconds WAITSHORT = new WaitForSeconds(0.2f);
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITTHREE = new WaitForSeconds(3f);
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    GameManager gameManager;

    List<string> levelEndTexts;

    AnalyticsController analytics;

    string rightAnswerText = "";
    string leftAnswerText = "";

    void Start()
    {
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();

        
        if (gameManager)
        {
            titleSeen = gameManager.IsTitleSeen();
            analytics.LevelStarted();
        }
        else
        {
            titleSeen = true;
        }
               
        gameController = GameController.GetController();
        characterController = gameController.GetCharacterController();

        emojiButton1.onClick.AddListener(delegate { RightAnswer(); });
        emojiButton2.onClick.AddListener(delegate { WrongAnswer(); });
        emojiButton3.onClick.AddListener(delegate { WrongAnswer(); });

        nextLeveButton.onClick.AddListener(delegate { NextLevel(); });
        resetLevelButton.onClick.AddListener(delegate { ResetLevel(); });
        if (gameManager)
        {
            currentLevel = gameManager.GetlevelCount();
        }
        else
        {
            currentLevel = 0;
        }
        
        GetLevelData();
        gameController.GetCancelMeter().SetCancelMeter(totalSegments);
        if (titleSeen)
        {
            Invoke("PlayerEntry", 1f);
            Invoke("NextConversation", 3f);
            //NextConversation();
            titlePanel.SetActive(false);
            tutorialPanel.SetActive(false);
        }
        else
        {
            titlePanel.SetActive(true);
            tutorialPanel.SetActive(true);
        }
        
        dayCount.text = "DAY " + (gameManager.GetLevelProgressionCount() + 1);

        startButton.onClick.AddListener(delegate {
            Invoke("PlayerEntry", 1f);
            Invoke("NextConversation", 3f);
            //NextConversation();
            gameManager.TitleSeen();
            titlePanel.SetActive(false);
        });

        levelEndTexts = new List<string>();
        levelEndTexts.Add("Met an awesome girl today. Feeling blessed!");
        levelEndTexts.Add("Yo! Check out my date for tonight, she's rocking!!");
        levelEndTexts.Add("Fam, I was smooth enough for this hottie!!");
    }
    void PlayerEntry()
    {
        characterController.PlayerEntry();
    }
    void GetLevelData() { levelQuestions = gameManager.GetLevelQuestions(currentLevel);  totalSegments = levelQuestions.questions.Count; }

    public void NextConversation( )
    {
        //pickUpLineStartPanel.SetActive(true);
        AnimatePanel(pickUpLineStartPanel, playerHead);
        npcResponsePanel.SetActive(false);
        whyPanel.SetActive(false);

        characterController.PlayerTalk();

        

        if (segmentCount > totalSegments - 1)
        {
            // level end

            //if (point >= levelQuestions.strikesToWin)
            if (point >= totalSegments - 1)
            {

                //Debug.Log("strikes complete!! Level end");
                LevelComplete();
            }
            //else
            //{
            //    Debug.Log("Level end");
            //    LevelFailed();
            //}
        }
        else
        {
            //upperAnswerButton.gameObject.SetActive(true);
            //lowerAnswerButton.gameObject.SetActive(true);

            //AnimatePanel(upperAnswerButton.gameObject);
            //AnimatePanel(lowerAnswerButton.gameObject);

            rightAnswerText = "";
            leftAnswerText = "";

            //if (isPreviousRight) { }
            //else { }

            playerLinesText.ShowMessage(levelQuestions.questions[segmentCount].convoStart);

            rightAnswerText = levelQuestions.questions[segmentCount].convoEndRight;
            leftAnswerText = levelQuestions.questions[segmentCount].ConvoEndLeft;



            //if (Random.Range(0, 100) > 50)
            //{
            //    // is upper right answer
            //    upperAnswerText.text = rightAnswerText;
            //    lowerAnswerText.text = leftAnswerText;

            //    emojiImage1.sprite = levelQuestions.questions[segmentCount].rightSprite;
            //    emojiImage2.sprite = levelQuestions.questions[segmentCount].leftSprite;
            //    emojiImage3.sprite = levelQuestions.questions[segmentCount].leftSpriteExtra;

            //    isUpperRightAnswer = true;
            //}
            //else
            //{
            //    // is lower right answer
            //    upperAnswerText.text = leftAnswerText;
            //    lowerAnswerText.text = rightAnswerText;

            //    emojiImage1.sprite = levelQuestions.questions[segmentCount].leftSprite;
            //    emojiImage2.sprite = levelQuestions.questions[segmentCount].rightSprite;
            //    emojiImage3.sprite = levelQuestions.questions[segmentCount].leftSpriteExtra;

            //    isUpperRightAnswer = false;
            //}

            

            emojiImage1.sprite = levelQuestions.questions[segmentCount].rightSprite;
            emojiImage2.sprite = levelQuestions.questions[segmentCount].leftSprite;
            emojiImage3.sprite = levelQuestions.questions[segmentCount].leftSpriteExtra;

            emojiText1.text = levelQuestions.questions[segmentCount].rightSpriteText;
            emojiText2.text = levelQuestions.questions[segmentCount].leftSpriteText;
            emojiText3.text = levelQuestions.questions[segmentCount].leftSpriteExtraText;

            isUpperRightAnswer = true;

            answerText.ShowMessage("");


            ShuffleButton();

            NPCWhy();
        }


        segmentCount++;
    }
    void AnswerCheck()
    {
        TutorialCheck();
        characterController.PlayerTalk();

        emojiButton1.transform.DOKill();
        emojiButton2.transform.DOKill();
        emojiButton3.transform.DOKill();

        emojiButton1.gameObject.SetActive(false);
        emojiButton2.gameObject.SetActive(false);
        emojiButton3.gameObject.SetActive(false);

        emojiBGtray.SetActive(false);


        //if (isUpper)
        //{
        //    if (isUpperRightAnswer)
        //    {
        //        isPreviousRight = true;
        //        point++;
        //        NPCResposne(levelQuestions.questions[segmentCount - 1].rightResponse);
        //        characterController.PlayerHappy();
        //        characterController.NPCHappy();
        //    }
        //    else
        //    {
        //        isPreviousRight = false;
        //        NPCResposne(levelQuestions.questions[segmentCount - 1].leftResponse);
        //        characterController.PlayerWrong();
        //        characterController.NPCWrong();
        //    }
        //}
        //else
        //{
        //    if (isUpperRightAnswer)
        //    {
        //        isPreviousRight = false;
        //        NPCResposne(levelQuestions.questions[segmentCount - 1].leftResponse);
        //        characterController.PlayerWrong();
        //        characterController.NPCWrong();
        //    }
        //    else
        //    {
        //        isPreviousRight = true;
        //        point++;
        //        NPCResposne(levelQuestions.questions[segmentCount - 1].rightResponse);
        //        characterController.PlayerHappy();
        //        characterController.NPCHappy();
        //    }
        //}
        
        
    }
    void RightAnswer()
    {
        StartCoroutine(RightAnswerRoutine());        
    }
    void WrongAnswer()
    {
        StartCoroutine(WrongAnswerRoutine());        
    }
    IEnumerator RightAnswerRoutine()
    {
        AnswerCheck();
        answerText.SetColor(Color.blue);
        answerText.ShowMessage(rightAnswerText);
        isPreviousRight = true;
        point++;
        gameController.GetCancelMeter().UpdateMeterValue(point, segmentCount - 1, isPreviousRight);
        yield return WAITTWO;
        NPCResposne(levelQuestions.questions[segmentCount - 1].rightResponse);
        characterController.PlayerHappy();
        characterController.NPCHappy();
    }
    IEnumerator WrongAnswerRoutine()
    {
        AnswerCheck();
        answerText.SetColor(Color.red);
        answerText.ShowMessage(leftAnswerText);
        isPreviousRight = false;
        gameController.GetCancelMeter().UpdateMeterValue(point, segmentCount - 1, isPreviousRight);
        yield return WAITTWO;
        NPCResposne(levelQuestions.questions[segmentCount - 1].leftResponse);
        characterController.PlayerWrong();
        characterController.NPCWrong();
    }

    void ShuffleButton()
    {
        int rand = Random.Range(0, 100);
        if (rand > 50)
        {
            emojiButton1.transform.SetSiblingIndex(1);
            emojiButton2.transform.SetSiblingIndex(2);
            emojiButton3.transform.SetSiblingIndex(0);
        }
        else
        {
            emojiButton1.transform.SetSiblingIndex(2);
            emojiButton2.transform.SetSiblingIndex(1);
            emojiButton3.transform.SetSiblingIndex(0);
        }
    }

    void NPCResposne(string response)
    {
        StartCoroutine(NPCResponseRoutine(response));
    }
    IEnumerator NPCResponseRoutine(string _response)
    {
        pickUpLineStartPanel.SetActive(false);
        whyPanel.SetActive(false);
        AnimatePanel(npcResponsePanel, npcHead);
        yield return ENDOFFRAME;
        npcResponceText.ShowMessage(_response);

        yield return WAITTHREE;
        if (isPreviousRight)
        {
            NextConversation();
        }
        else
        {
            //Debug.Log("Level end");
            LevelFailed();
        }
        
    }
    void NPCWhy()
    {
        StartCoroutine(WhyPaneRoutine());
    }
    IEnumerator WhyPaneRoutine()
    {
        yield return WAITONE;
        characterController.NPCTalk();
        //whyPanel.SetActive(true);
        AnimatePanel(whyPanel, npcHead);
        yield return ENDOFFRAME;
        whyText.ShowMessage(levelQuestions.questions[segmentCount - 1].convoStartWhy);
        yield return WAITHALF;

        
        characterController.NPCTalk();
        AnimatePanel(emojiBGtray);
        yield return WAITSHORT;
        AnimatePanel(emojiButton1.gameObject);
        //yield return WAITSHORT;
        AnimatePanel(emojiButton2.gameObject);
        //yield return WAITSHORT;
        if (levelQuestions.questions[segmentCount - 1].leftSpriteExtra != null)
        {
            AnimatePanel(emojiButton3.gameObject);
        }
        
    }
    void NPCJerk()
    {
        StartCoroutine(JerkPanelRoutine());
    }
    IEnumerator JerkPanelRoutine()
    {
        yield return WAITONE;
        AnimatePanel(whyPanel, npcHead);
        yield return ENDOFFRAME;
        whyText.ShowMessage("You Jerk!!");
        pickUpLineStartPanel.SetActive(false);
        yield return WAITONE;
        AnimatePanel(pickUpLineStartPanel, playerHead);
        answerText.gameObject.SetActive(false);
        playerLinesText.ShowMessage(levelQuestions.levelEndLeft);
        whyPanel.SetActive(false);
    }

    void LevelComplete()
    {
        StartCoroutine(LevelCompleteRoutine());
    }
    IEnumerator LevelCompleteRoutine()
    {
        characterController.PlayerVictory();
        characterController.NPCVictory();

       

        pickUpLineStartPanel.SetActive(true);
        npcResponsePanel.SetActive(false);

        emojiButton1.gameObject.SetActive(false);
        emojiButton2.gameObject.SetActive(false);
        emojiButton3.gameObject.SetActive(false);

        int cc = celebrateParticles.Count;
        celebrateParticles[Random.Range(0, cc)].Play();

        gameController.GetCancelMeter().CompleteBar();
        //npcResponceText.ShowMessage(levelQuestions.levelEndRight);
        answerText.gameObject.SetActive(false);
        playerLinesText.ShowMessage(levelQuestions.levelEndRight);
        yield return WAITTWO;
        pickUpLineStartPanel.gameObject.SetActive(false);
        gameController.GetCaptureScreen().SelfieMode();
        //yield return WAITONE;

        

    }
    public void SelfieComplete()
    {
        levelCompleteText.text = levelEndTexts[Random.Range(0, levelEndTexts.Count)];
        likes.text = "" + Random.Range(90, 200);
        comments.text = "" + Random.Range(90, 200);
        shares.text = "" + Random.Range(90, 200);

        screenShotImage.sprite = gameController.GetCaptureScreen().GetScreenShot();

        AnimatePanel(levelCompletePanel);
    }
    void LevelFailed()
    {
        StartCoroutine(LevelFailedRoutine());
    }
    IEnumerator LevelFailedRoutine()
    {
        NPCJerk();
        characterController.PlayerDefeat();
        characterController.NPCDefeat();
        //yield return WAITONE;
        pickUpLineStartPanel.SetActive(true);
        npcResponsePanel.SetActive(false);

        emojiButton1.gameObject.SetActive(false);
        emojiButton2.gameObject.SetActive(false);
        emojiButton3.gameObject.SetActive(false);

        //npcResponceText.ShowMessage(levelQuestions.levelEndRight);
        //playerLinesText.ShowMessage(levelQuestions.levelEndLeft);

        pickUpLineStartPanel.SetActive(false);

        yield return WAITTHREE;
        AnimatePanel(levelFailedPanel);
    }

    void NextLevel()
    {
        //SceneManager.LoadScene(0);
        gameManager.GotoNextStage();
        analytics.LevelEnded();
    }
    void ResetLevel()
    {
        //SceneManager.LoadScene(0);
        gameManager.ResetStage();
        analytics.LevelFailed();
    }
    void AnimatePanel(GameObject panel, Transform startPos = null)
    {
        bool isButton = false;
        Button bb = null;
        if (panel.GetComponent<Button>())
            isButton = true;
        else
            isButton = false;

        if (isButton)
        {
            bb = panel.GetComponent<Button>();
            bb.interactable = false;
        }
        panel.SetActive(true);
        Vector3 currentPos = panel.transform.position;
        
        if (startPos)
        {
            panel.transform.position = startPos.position;
            panel.transform.DOMove(currentPos, 0.3f).SetEase(Ease.OutSine);
        }
        
        panel.transform.localScale = Vector3.one * 0.5f;
        panel.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutSine).OnComplete(() => EnableButton(isButton, bb) ) ;
    }
    void EnableButton(bool _isButton, Button bb)
    {
        if (_isButton) { bb.interactable = true; }
    }

    void TutorialCheck()
    {
        if (tutorialPanel.activeInHierarchy)
        {
            tutorialPanel.SetActive(false);
        }
    }
}
