﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkEnvironment : MonoBehaviour
{
    public bool add = false;

    [SerializeField] List<Vector3> positions;
    [SerializeField] List<Vector3> rotations;

    
    void Start()
    {
        //positions = new List<Vector3>();
        //rotations = new List<Vector3>();
        StagePostion temp = GetStageRandom();
        transform.position = temp.positions;
        transform.eulerAngles = temp.rotations;
    }

    // Update is called once per frame
    void Update()
    {
        if (add)
        {
            //AddTransformValue();
            StagePostion temp = GetStageRandom();
            transform.position = temp.positions;
            transform.eulerAngles = temp.rotations;
            add = false;
        }
    }

    void AddTransformValue()
    {
        positions.Add(transform.position);
        rotations.Add(transform.eulerAngles);
    }
    public StagePostion GetStageRandom()
    {
        int rand = Random.Range(0, positions.Count);
        StagePostion temp;
        temp.positions = positions[rand];
        temp.rotations = rotations[rand];

        return temp;
    }

    
}
public struct StagePostion
{
    public Vector3 positions;
    public Vector3 rotations;
}
